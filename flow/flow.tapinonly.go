package flow

import (
	"fmt"
	"os"

	"bitbucket.org/billing/go-recon/app"
	"bitbucket.org/billing/go-recon/handler"
)

// TapinOnlyRecon :
func TapinOnlyRecon() error {
	fmt.Printf("[ Getting Jobs TAPIN Only From Database ] : ")
	handlerProcess := handler.Process{}
	listProcess, err := handlerProcess.GetProcessTapinOnly()
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	if listProcess == nil {
		fmt.Println("No job found in database")
		return nil
	}

	fmt.Println(len(listProcess), "Jobs")

	fmt.Printf("[ Getting Data Structure From Database ] : ")
	dataStructure, err := handlerProcess.GetDataStructure()
	if err != nil {
		fmt.Println("Error =>", err.Error())
		return err
	}

	fmt.Println("Success")

	fmt.Printf("[ Getting Compare Configuration From Database ] : ")
	levelVolcomp, err := handlerProcess.GetLevelVolcompTapinOnly()
	if err != nil {
		fmt.Println("Error =>", err.Error())
		return err
	}

	if levelVolcomp == nil {
		fmt.Println("No Compare Configuration found in database")
		return nil
	}

	fmt.Println("Success")

	for _, v := range listProcess {
		fmt.Println("[ Processing ] : { Process ID :", v.ProcessID, "}")
		// handlerProcess.UpdateStatus(v.ProcessID, "4")
		listTapinOnly, err := handlerProcess.GetListTapinOnly(v.Periode, v.MoMt, v.PostPre)
		if err != nil {
			fmt.Println(err.Error())
			// hProcess := handler.Process{ProcessStatusID: 5, ErrorMessage: err.Error()}
			// hProcess.CloseProcess(&v)
			continue
		}
		listPathDBOCS, err := handlerProcess.GetSlicedProcess(v.MinStartcall, v.MaxStartcall, "1", v.MoMt, v.PostPre)
		if err != nil {
			fmt.Println(err.Error())
			// hProcess := handler.Process{ProcessStatusID: 5, ErrorMessage: err.Error()}
			// hProcess.CloseProcess(&v)
			continue
		}

		currentDBPath := app.Appl.Configs.Main.ProcessDir + v.ProcessID + "/"

		handlerRecon := handler.ReconTapinOnly{
			ListPathDBOCS:     &listPathDBOCS,
			CurrentDBPath:     currentDBPath,
			ListProcess:       &v,
			LevelVolcomp:      &levelVolcomp,
			DataStructure:     &dataStructure,
			ListTapinOnlyFile: &listTapinOnly,
		}
		err = handlerRecon.CombineDB()
		if err != nil {
			// hProcess := handler.Process{ProcessStatusID: 5, ErrorMessage: err.Error()}
			// hProcess.CloseProcess(&v)

			os.RemoveAll(currentDBPath)
			continue
		}

		handlerRecon.ProcessRecon()

		// processResult.CloseProcess(&v)

		os.RemoveAll(currentDBPath)
		os.Exit(3)
	}
	return nil
}
