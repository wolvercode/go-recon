package handler

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/billing/go-recon/common"
	"bitbucket.org/billing/go-recon/model"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/util"
)

// ReconTapinOnly : Handler Recon Tapin Only
type ReconTapinOnly struct {
	DBOCS                *leveldb.DB
	ListPathDBOCS        *[]model.SlicedProcess
	CurrentDBPath        string
	ListProcess          *model.ProcessReconTapinOnly
	LevelVolcomp         *[]model.LevelVolcomp
	DataStructure        *model.DataStructure
	ListTapinOnlyFile    *[]model.ListTapinOnly
	countInp             int
	countMatch           int
	countTapinOnly       int
	matchLine            map[string]map[int]int
	matchKey             map[string]map[string]int
	mapOpenFile          map[int]*os.File
	mapFileWriter        map[int]*bufio.Writer
	mapCountTapinOnly    map[string]map[string]int
	mapCountMatchPerFile map[string]map[int]int
	mapCountMatchPerTol  map[int]int
}

// CombineDB :
func (p *ReconTapinOnly) CombineDB() error {
	fmt.Printf("  > [ Combining Key OCS For Compare ] : ")
	var err error
	// Opening Combined Database OCS
	p.DBOCS, err = leveldb.OpenFile(p.CurrentDBPath, nil)
	if err != nil {
		fmt.Println("Failed To Open LevelDB, with error =>", err.Error())
		return err
	}

	// Looping every OCS database
	counts := 0
	for _, v := range *p.ListPathDBOCS {
		// Opening OCS database
		subDbConn, err := leveldb.OpenFile(v.PathDB, nil)
		vmatch := strings.TrimSuffix(v.PathDB, "db/") + "match/"
		if err != nil {
			fmt.Println("Failed To Open LevelDB, with error =>", err.Error())
			continue
		}
		// Looping every OCS Database key
		iter := subDbConn.NewIterator(nil, nil)
		for iter.Next() {
			counts++
			// Putting OCS Database key to Combined Database
			err = p.DBOCS.Put([]byte(string(iter.Key())+"-"+vmatch), iter.Value(), nil)
		}
		iter.Release()
		// Closing OCS database
		subDbConn.Close()
	}
	fmt.Println("Success, Total Key =>", counts)

	return nil
}

// ProcessRecon :
func (p *ReconTapinOnly) ProcessRecon() error {
	fmt.Println("  > [ Processing Recon TAPIN Only vs OCS ]")

	// Initializing map for Line & Key Match
	p.matchLine = make(map[string]map[int]int)
	p.matchKey = make(map[string]map[string]int)

	// Initializing map for Opened File & Buffered Writer
	p.mapOpenFile = make(map[int]*os.File)
	p.mapFileWriter = make(map[int]*bufio.Writer)

	// Match intitialization, making directory & file Match bufio writer
	if _, err := os.Stat(p.ListProcess.MatchTapinOnlyPath); os.IsNotExist(err) {
		fmt.Printf("    > [ Making Match Directory ] : ")
		err = os.MkdirAll(p.ListProcess.MatchTapinOnlyPath, 0777)
		if err != nil {
			fmt.Println("Failed, with error =>", err.Error())
			// hProcess.ProcessStatusID, hProcess.ErrorMessage = 5, err.Error()
			// return hProcess
			return err
		}
		fmt.Println("Success")
	}

	// Opening & Creating Match File
	inputMatch := p.ListProcess.MatchTapinOnlyPath + p.ListProcess.MatchTapinOnlyFilename
	fmt.Println("    > [ Match File ] : " + inputMatch)
	fileMatch, err := os.Create(inputMatch)
	if err != nil {
		fmt.Println("    > Failed, with error =>", err.Error())
		// hProcess.ProcessStatusID, hProcess.ErrorMessage = 5, err.Error()
		// return hProcess
		return err
	}

	// Bufio Writer Match File
	writerMatch := bufio.NewWriter(fileMatch)

	// Looping Level Volume Compare
	for _, vlevelVolcomp := range *p.LevelVolcomp {
		fmt.Println("    > [ Compare Level ] :", vlevelVolcomp.Level)

		urutStartcall, _ := strconv.Atoi(vlevelVolcomp.Conf["TAPIN_ONLY"].ParameterToleransi[0])
		urutDurasi, _ := strconv.Atoi(vlevelVolcomp.Conf["TAPIN_ONLY"].ParameterToleransi[1])

		// Creating Match File Per Toleransi
		fmt.Printf("      > [ Creating Match File Toleransi ] : ")
		for _, vToleransi := range vlevelVolcomp.Toleransi {
			var err error
			inputMatch := p.ListProcess.MatchTapinOnlyPath + (strings.Split(p.ListProcess.MatchTapinOnlyFilename, "."))[0] + "_" +
				strconv.Itoa(vToleransi.Startcall) + "_" + strconv.Itoa(vToleransi.Durasi) + "." + (strings.Split(p.ListProcess.MatchTapinOnlyFilename, "."))[1]
			p.mapOpenFile[vToleransi.IDToleransi], err = os.Create(inputMatch)
			if err != nil {
				fmt.Println("Failed, with error =>", err.Error())
				// hProcess.ProcessStatusID, hProcess.ErrorMessage = 5, err.Error()
				// return hProcess
				return err
			}
			// Bufio Writer Match File
			p.mapFileWriter[vToleransi.IDToleransi] = bufio.NewWriter(p.mapOpenFile[vToleransi.IDToleransi])
		}
		fmt.Println("Success")

		p.mapCountMatchPerFile = make(map[string]map[int]int)
		p.mapCountMatchPerTol = make(map[int]int)

		for _, vt := range *p.ListTapinOnlyFile {
			// Opening Input Tapin Only File
			inputTapinOnly := vt.PathFile + vt.Filename

			p.countInp = 0

			p.matchLine[vt.BatchIDAsal] = make(map[int]int)
			p.mapCountMatchPerFile[vt.BatchIDAsal] = make(map[int]int)

			fmt.Printf("    > [ Tapin Only ] : { BatchID Asal : %s, File : %s }\n", vt.BatchIDAsal, inputTapinOnly)
			fileTapinOnly, err := os.OpenFile(inputTapinOnly, os.O_RDONLY, os.ModePerm)
			if err != nil {
				fmt.Println("Failed, with error =>", err.Error())
				// hProcess.ProcessStatusID, hProcess.ErrorMessage = 5, err.Error()
				// return hProcess
				return err
			}
			defer fileTapinOnly.Close()

			// Looping Undup Tapin File Line by Line
			sc := bufio.NewScanner(fileTapinOnly)
			for sc.Scan() {
				//  Doing Recon Line by Line
				p.lineRecon(sc, &vlevelVolcomp, writerMatch, urutStartcall, urutDurasi, vt.BatchIDAsal)
			}
			if err := sc.Err(); err != nil {
				fmt.Println("Failed, with error =>", err.Error())
				// hProcess.ProcessStatusID, hProcess.ErrorMessage = 5, err.Error()
				// return hProcess
				return err
			}

			for k, vToleransi := range vlevelVolcomp.Toleransi {
				if p.mapCountMatchPerFile[vt.BatchIDAsal][vToleransi.IDToleransi] > 0 {
					fmt.Printf("      > [ Match Toleransi StartCall : %d seconds, Durasi : %d seconds ] : %d records\n", vlevelVolcomp.Toleransi[k].Startcall, vlevelVolcomp.Toleransi[k].Durasi, p.mapCountMatchPerFile[vt.BatchIDAsal][vToleransi.IDToleransi])
				}
			}
		}

		// Closing Match File Per Toleransi
		for k, vToleransi := range vlevelVolcomp.Toleransi {
			err := p.mapFileWriter[vToleransi.IDToleransi].Flush()
			if err != nil {
				fmt.Println("Failed, with error =>", err.Error())
			}
			p.mapOpenFile[vToleransi.IDToleransi].Close()

			fmt.Printf("    > [ Total Match Toleransi StartCall : %d seconds, Durasi : %d seconds ] : %d records\n", vlevelVolcomp.Toleransi[k].Startcall, vlevelVolcomp.Toleransi[k].Durasi, p.mapCountMatchPerTol[vToleransi.IDToleransi])
		}
	}

	// File Match Bufio Flush
	err = writerMatch.Flush()
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		// hProcess.ProcessStatusID, hProcess.ErrorMessage = 5, err.Error()
		// return hProcess
		return err
	}
	fileMatch.Close()

	fmt.Println("    > [ Total Match All Toleransi ] :", p.countMatch)

	// // Writing Tapin Only
	// err = p.WritingTapinOnly()
	// if err != nil {
	// 	// hProcess.ProcessStatusID, hProcess.ErrorMessage = 5, err.Error()
	// 	// return hProcess
	// 	return err
	// }

	return nil
}

func (p *ReconTapinOnly) lineRecon(sc *bufio.Scanner, vlevelVolcomp *model.LevelVolcomp, writerMatch *bufio.Writer, urutStartcall, urutDurasi int, batchIDAsal string) int {
	var keydup string

	countMatch := 0

	p.countInp++
	if _, ok := p.matchLine[batchIDAsal][p.countInp]; ok {
		return countMatch
	}

	r := strings.NewReplacer(`"`, "", "Event: ", "")

	columns := strings.Split(strings.Replace(sc.Text(), "+", "", -1), ",")
	for _, vParamCompare := range vlevelVolcomp.Conf["TAPIN_ONLY"].ParameterCompare {
		urutan, _ := strconv.Atoi(vParamCompare)
		keydup += columns[urutan]
		keydup = r.Replace(keydup)
	}

	startcallColumns, err := common.ParseDatetime("20060102150405", r.Replace(columns[urutStartcall]))
	if err != nil {
		fmt.Println(err.Error())
		return countMatch

	}

	durColumns, _ := strconv.Atoi(columns[urutDurasi])

	countMatch = p.SearchWithTolerance(columns, keydup, writerMatch, sc, startcallColumns, durColumns, vlevelVolcomp, batchIDAsal)

	return countMatch
}

// SearchWithTolerance :
func (p *ReconTapinOnly) SearchWithTolerance(columns []string, keydup string, writerMatch *bufio.Writer, sc *bufio.Scanner, startcallColumns time.Time, durColumns int, vlevelVolcomp *model.LevelVolcomp, batchIDAsal string) int {
	countMatch := 0

	for _, vToleransi := range vlevelVolcomp.Toleransi {
		iter := p.DBOCS.NewIterator(util.BytesPrefix([]byte(keydup+"-")), nil)

		for iter.Next() {
			// Use key/value.
			stringKey := string(iter.Key())
			splitKey := strings.Split(stringKey, "-")
			startcallKey, err := common.ParseDatetime("20060102150405", splitKey[1])
			if err != nil {
				fmt.Println(err.Error())
				return countMatch
			}
			diff := math.Abs(startcallColumns.Sub(startcallKey).Seconds())
			durKey, _ := strconv.Atoi(splitKey[2])
			diffDur := math.Abs(float64(durColumns) - float64(durKey))
			if int(diff) <= vToleransi.Startcall && int(diffDur) <= vToleransi.Durasi {
				valOCS := fmt.Sprintf("%s %d %d", string(iter.Value()), vToleransi.Startcall, vToleransi.Durasi)
				countMatch = p.OnMatchLine(splitKey, writerMatch, vToleransi.IDToleransi, sc.Text(), valOCS, batchIDAsal)
				p.mapCountMatchPerFile[batchIDAsal][vToleransi.IDToleransi]++
				iter.Release()
				return countMatch
			}
		}
		iter.Release()
	}

	return countMatch
}

// OnMatchLine :
func (p *ReconTapinOnly) OnMatchLine(splitKey []string, writerMatch *bufio.Writer, idToleransi int, valTAPIN, valOCS, batchIDAsal string) int {
	p.countMatch++
	p.mapCountMatchPerTol[idToleransi]++

	p.matchLine[batchIDAsal][p.countInp] = 1
	if p.matchKey[splitKey[3]] == nil {
		p.matchKey[splitKey[3]] = map[string]int{}
	}
	p.matchKey[splitKey[3]][strings.Join(splitKey[:len(splitKey)-1], "-")] = 1

	fmt.Fprintln(writerMatch, valTAPIN, valOCS)
	fmt.Fprintln(p.mapFileWriter[idToleransi], valTAPIN, valOCS)

	return 1
}

// WritingTapinOnly :
// func (p *ReconTapinOnly) WritingTapinOnly() error {
// 	fmt.Println("  > [ Writing TAPIN Only ]")

// 	for _, vt := range *p.ListTapinOnlyFile {
// 		inputTapinOnly := vt.PathFile + vt.Filename
// 	}

// 	p.mapCountTapinOnly = make(map[string]map[string]int)

// 	currentDataStructure := p.DataStructure.TypeData["TAPIN"]

// 	// Making Tapin Only Directories if not exists
// 	if _, err := os.Stat(p.ListProcess.PathOnly); os.IsNotExist(err) {
// 		fmt.Printf("    > [ Making TAPIN Only Directory ] : ")
// 		err = os.MkdirAll(p.ListProcess.PathOnly, 0777)
// 		if err != nil {
// 			fmt.Println("Failed, with error =>", err.Error())
// 			return err
// 		}
// 		fmt.Println("Success")
// 	}

// 	// Opening & Creating Tapin Only File
// 	inputOnly := p.ListProcess.MatchTapinOnlyPath + p.ListProcess.MatchTapinOnlyFilename
// 	fmt.Println("    > [ TAPIN Only File ] : " + inputOnly)
// 	fileOnly, err := os.Create(inputOnly)
// 	if err != nil {
// 		fmt.Println("    > Failed, with error =>", err.Error())
// 		return err
// 	}
// 	// Bufio Writer Tapin Only File
// 	writerOnly := bufio.NewWriter(fileOnly)
// 	// Opening Undup File
// 	fileUndup, err := os.OpenFile(inputUndup, os.O_RDONLY, os.ModePerm)
// 	if err != nil {
// 		fmt.Println("    > Failed, with error =>", err.Error())
// 		return err
// 	}
// 	defer fileUndup.Close()

// 	// Looping Undup Tapin File Line by Line
// 	sc := bufio.NewScanner(fileUndup)
// 	currLine := 0
// 	for sc.Scan() {
// 		currLine++
// 		// Doing Write Tapin Only for line that has no matchLine
// 		if _, ok := p.matchLine[currLine]; ok {
// 			continue
// 		}
// 		// Splitting current line to get field array
// 		columns := strings.Split(strings.Replace(sc.Text(), "+", "", -1), "|")

// 		calledNumber := columns[currentDataStructure.Parameter["OTHERTELNUM"].Urutan]
// 		operatorID := columns[currentDataStructure.Parameter["PARTNERID"].Urutan]
// 		callingNumber := columns[currentDataStructure.Parameter["MSISDN"].Urutan]
// 		tapFileName := columns[currentDataStructure.Parameter["TAP_FILENAME"].Urutan]
// 		sessionID := callingNumber + calledNumber + tapFileName + columns[currentDataStructure.Parameter["STARTTIME"].Urutan]
// 		utcTime := "+" + columns[currentDataStructure.Parameter["UTC_TIME"].Urutan]
// 		t, _ := time.Parse("20060102150405", columns[currentDataStructure.Parameter["STARTTIME"].Urutan])
// 		timeFormat := t.Format("2006/01/02-15-04-05")

// 		m := []string{
// 			0:  `"` + "Event: " + columns[currentDataStructure.Parameter["IMSI"].Urutan] + `"`,
// 			1:  `"` + "10" + `"`,
// 			2:  `"` + timeFormat + `"`,
// 			3:  `""`,
// 			4:  `""`,
// 			5:  `""`,
// 			6:  `""`,
// 			7:  `""`,
// 			8:  `""`,
// 			9:  `""`,
// 			10: `""`,
// 			11: `""`,
// 			12: `""`,
// 			13: `"` + calledNumber + `"`,
// 			14: `"` + operatorID + `"`,
// 			15: `"` + columns[currentDataStructure.Parameter["SERVICENAME"].Urutan] + `"`,
// 			16: `"` + columns[currentDataStructure.Parameter["TAP_FEE"].Urutan] + `"`,
// 			17: `"` + callingNumber + `"`,
// 			18: `""`,
// 			19: `"6"`,
// 			20: `"` + columns[currentDataStructure.Parameter["CALLEDDIGIT"].Urutan] + `"`,
// 			21: `"` + columns[currentDataStructure.Parameter["DURATION"].Urutan] + `"`,
// 			22: `"` + columns[currentDataStructure.Parameter["MSISDN"].Urutan] + `"`,
// 			23: `"` + sessionID + `"`,
// 			24: `""`,
// 			25: `"1"`,
// 			26: `"` + operatorID + `"`,
// 			// 27: `"` + columns[currentDataStructure.Parameter["STARTTIME"].Urutan] + `"`,
// 			27: `"` + columns[currentDataStructure.Parameter["LOCAL_TIME"].Urutan] + `"`,
// 			28: `""`,
// 			29: `""`,
// 			30: `""`,
// 			31: `""`,
// 			32: `"` + utcTime + `"`,
// 			33: `""`,
// 			34: `""`,
// 			35: `""`,
// 			36: `""`,
// 			37: `"` + tapFileName + `"`,
// 		}
// 		// Write Line to file Tapin Only
// 		fmt.Fprintln(writerOnly, strings.Join(m, ","))

// 		if p.mapCountTapinOnly[operatorID] == nil {
// 			p.mapCountTapinOnly[operatorID] = map[string]int{}
// 		}

// 		dur, _ := strconv.Atoi(columns[currentDataStructure.Parameter["DURATION"].Urutan])

// 		p.mapCountTapinOnly[operatorID]["durations"] += dur

// 		p.mapCountTapinOnly[operatorID]["records"]++

// 		p.countTapinOnly++
// 	}
// 	if err := sc.Err(); err != nil {
// 		fmt.Println("    > Failed, with error =>", err.Error())
// 		return err
// 	}

// 	// File Tapin Only Bufio Flush
// 	err = writerOnly.Flush()
// 	if err != nil {
// 		fmt.Println("    > Failed, with error =>", err.Error())
// 		return err
// 	}
// 	fileOnly.Close()

// 	return nil
// }
