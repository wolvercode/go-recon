package handler

import (
	"fmt"

	"bitbucket.org/billing/go-recon/model"
)

// Process :
type Process struct {
	ProcessStatusID            int                                       `json:"processstatus_id"`
	MinStartcall               int                                       `json:"min_startcall"`
	MaxStartcall               int                                       `json:"max_startcall"`
	TotalInput                 int                                       `json:"total_input"`
	TotalMatch                 int                                       `json:"total_match"`
	TotalTapinOnly             int                                       `json:"total_tapin_only"`
	ErrorMessage               string                                    `json:"error_message"`
	LogMatch                   map[int]map[string]int                    `json:"log_match"`
	LogTapinOnly               map[string]*model.LogMatchContent         `json:"log_tapinonly"`
	LogMatchPartnerID          map[string]*model.LogMatchContent         `json:"log_match_partnerid"`
	LogMatchToleransiPartnerID map[string]map[int]*model.LogMatchContent `json:"log_match_toleransi_partnerid"`
}

var modelProcess model.ProcessRecon

var modelProcessTapinOnly model.ProcessReconTapinOnly

// GetProcess :
func (p *Process) GetProcess() ([]model.ProcessRecon, error) {
	output, err := modelProcess.GetProcess()
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return output, nil
}

// UpdateStatus :
func (p *Process) UpdateStatus(processID, processStatusID string) error {
	err := modelProcess.UpdateStatus(processID, processStatusID)
	if err != nil {
		fmt.Println("  > [ Update Status Failed ] :", err.Error())
		return err
	}
	return nil
}

// CloseProcess :
func (p *Process) CloseProcess(inputModelProcess *model.ProcessRecon) error {
	closeStruct := model.ClosingStruct{
		ProcessConf:                inputModelProcess,
		TotalInput:                 p.TotalInput,
		TotalMatch:                 p.TotalMatch,
		TotalTapinOnly:             p.TotalTapinOnly,
		ProcessStatusID:            p.ProcessStatusID,
		ErrorMessage:               p.ErrorMessage,
		LogMatch:                   p.LogMatch,
		LogTapinOnly:               p.LogTapinOnly,
		LogMatchPartnerID:          p.LogMatchPartnerID,
		LogMatchToleransiPartnerID: p.LogMatchToleransiPartnerID,
	}
	err := modelProcess.CloseProcess(&closeStruct)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	return nil
}

// GetSlicedProcess :
func (p *Process) GetSlicedProcess(minStartcall, maxStartcall, dataTypeID, moMt, postPre string) ([]model.SlicedProcess, error) {
	output, err := modelProcess.GetSlicedProcess(minStartcall, maxStartcall, dataTypeID, moMt, postPre)
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return output, nil
}

// GetLevelVolcomp :
func (p *Process) GetLevelVolcomp() ([]model.LevelVolcomp, error) {
	output, err := modelProcess.GetLevelVolcomp()
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return output, nil
}

// GetDataStructure :
func (p *Process) GetDataStructure() (model.DataStructure, error) {
	output, err := modelProcess.GetDataStructure()
	if err != nil {
		fmt.Println(err.Error())
		return output, err
	}
	return output, nil
}

// GetProcessTapinOnly :
func (p *Process) GetProcessTapinOnly() ([]model.ProcessReconTapinOnly, error) {
	output, err := modelProcess.GetProcessTapinOnly()
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return output, nil
}

// GetListTapinOnly :
func (p *Process) GetListTapinOnly(periode, moMt, postPre string) ([]model.ListTapinOnly, error) {
	output, err := modelProcess.GetListTapinOnly(periode, moMt, postPre)
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return output, nil
}

// GetLevelVolcompTapinOnly :
func (p *Process) GetLevelVolcompTapinOnly() ([]model.LevelVolcomp, error) {
	output, err := modelProcess.GetLevelVolcompTapinOnly()
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return output, nil
}
