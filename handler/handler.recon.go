package handler

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/billing/go-recon/common"
	"bitbucket.org/billing/go-recon/model"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/util"
)

// Recon : Handler Recon
type Recon struct {
	DBOCS                           *leveldb.DB
	DBOCSMatch                      *leveldb.DB
	ListPathDBOCS                   *[]model.SlicedProcess
	CurrentDBPath                   string
	ListProcess                     *model.ProcessRecon
	LevelVolcomp                    *[]model.LevelVolcomp
	DataStructure                   *model.DataStructure
	countInp                        int
	countMatch                      int
	countTapinOnly                  int
	matchLine                       map[int]int
	matchKey                        map[string]map[string]int
	mapOpenFile                     map[int]*os.File
	mapFileWriter                   map[int]*bufio.Writer
	mapCountMatch                   map[int]map[string]int
	mapCountTapinOnly               map[string]*model.LogMatchContent
	mapCountMatchPartnerID          map[string]*model.LogMatchContent
	mapCountMatchToleransiPartnerID map[string]map[int]*model.LogMatchContent
	countInpTotal                   int
}

// KeyStruct : OCS Key Struct
type KeyStruct struct {
	KeyCompare         string
	StartCall          string
	Duration           string
	MatchPath          string
	KeyExceptMatchPath string
}

// CombineDB :
func (p *Recon) CombineDB() error {
	fmt.Printf("  > [ Combining Key OCS For Compare ] : ")
	var err error
	// Opening Combined Database OCS
	p.DBOCS, err = leveldb.OpenFile(p.CurrentDBPath, nil)
	if err != nil {
		fmt.Println("Failed To Open Temporary OCS Database, with error =>", err.Error())
		return err
	}

	// Opening Combined Database OCS Match
	p.DBOCSMatch, err = leveldb.OpenFile(p.CurrentDBPath+"match/", nil)
	if err != nil {
		fmt.Println("Failed To Open Temporary OCS Match Database, with error =>", err.Error())
		return err
	}

	// Looping every OCS database
	countsDB := 0
	countsMatch := 0
	for _, v := range *p.ListPathDBOCS {
		// Opening OCS Database
		subDbConn, err := leveldb.OpenFile(v.PathDB, nil)
		if err != nil {
			fmt.Println("Failed To Open OCS Database, with error =>", err.Error())
			continue
		}
		matchPathKey := strings.TrimSuffix(v.PathDB, "db/") + "match/"

		// Looping every OCS Database key
		iter := subDbConn.NewIterator(nil, nil)
		for iter.Next() {
			countsDB++
			// Putting OCS Database key to Combined Database
			err = p.DBOCS.Put([]byte(string(iter.Key())+"-"+matchPathKey), iter.Value(), nil)
		}
		iter.Release()

		// Closing OCS database
		subDbConn.Close()

		// Opening OCS Match Database
		subDbConnMatch, err := leveldb.OpenFile(matchPathKey, nil)
		if err != nil {
			fmt.Println("Failed To Open OCS Match Database, with error =>", err.Error())
			continue
		}

		// Looping every OCS Database key
		iterMatch := subDbConnMatch.NewIterator(nil, nil)
		for iterMatch.Next() {
			countsMatch++
			// Putting OCS Database key to Combined Database
			err = p.DBOCSMatch.Put([]byte(string(iterMatch.Key())+"-"+matchPathKey), iterMatch.Value(), nil)
		}
		iterMatch.Release()

		// Closing OCS Match database
		subDbConnMatch.Close()
	}
	fmt.Printf("Success => { Total Key Database : %d, Total Key Match Database : %d }\n", countsDB, countsMatch)

	return nil
}

// ProcessRecon :
func (p *Recon) ProcessRecon() Process {
	var hProcess Process

	fmt.Println("  > [ Processing Recon TAPIN vs OCS ]")

	// Opening Undup Tapin File
	inputUndup := p.ListProcess.PathUndup + p.ListProcess.FilenameUndup
	fmt.Println("    > [ TAPIN File ] : " + inputUndup)

	// Initializing map for Line & Key Match
	p.matchLine = make(map[int]int)
	p.matchKey = make(map[string]map[string]int)
	p.mapCountMatch = make(map[int]map[string]int)
	p.mapCountMatchPartnerID = make(map[string]*model.LogMatchContent)
	p.mapCountMatchToleransiPartnerID = make(map[string]map[int]*model.LogMatchContent)

	// Match intitialization, making directory & file Match bufio writer
	if _, err := os.Stat(p.ListProcess.PathMatch); os.IsNotExist(err) {
		fmt.Printf("    > [ Making Match Directory ] : ")
		err = os.MkdirAll(p.ListProcess.PathMatch, 0777)
		if err != nil {
			fmt.Println("Failed, with error =>", err.Error())
			hProcess.ProcessStatusID, hProcess.ErrorMessage = 5, err.Error()
			return hProcess
		}
		fmt.Println("Success")
	}

	fmt.Println("  > [ Doing Recon Compare ]")
	// Opening & Creating Match File
	inputMatch := p.ListProcess.PathMatch + p.ListProcess.FilenameMatch
	fmt.Println("    > [ Match File ] : " + inputMatch)
	fileMatch, err := os.Create(inputMatch)
	if err != nil {
		fmt.Println("    > Failed, with error =>", err.Error())
		hProcess.ProcessStatusID, hProcess.ErrorMessage = 5, err.Error()
		return hProcess
	}
	// Bufio Writer Match File
	writerMatch := bufio.NewWriter(fileMatch)

	p.mapOpenFile = make(map[int]*os.File)
	p.mapFileWriter = make(map[int]*bufio.Writer)

	// Looping Level Volume Compare
	for _, vlevelVolcomp := range *p.LevelVolcomp {
		fmt.Println("    > [ Compare Level ] :", vlevelVolcomp.Level)

		urutStartcall, _ := strconv.Atoi(vlevelVolcomp.Conf["TAPIN"].ParameterToleransi[0])
		urutDurasi, _ := strconv.Atoi(vlevelVolcomp.Conf["TAPIN"].ParameterToleransi[1])

		// Creating Match File Per Toleransi
		fmt.Printf("      > [ Creating Match File Toleransi ] : ")
		for _, vToleransi := range vlevelVolcomp.Toleransi {
			var err error
			inputMatch := p.ListProcess.PathMatch + (strings.Split(p.ListProcess.FilenameMatch, "."))[0] + "_" +
				strconv.Itoa(vToleransi.Startcall) + "_" + strconv.Itoa(vToleransi.Durasi) + "." + (strings.Split(p.ListProcess.FilenameMatch, "."))[1]
			p.mapOpenFile[vToleransi.IDToleransi], err = os.Create(inputMatch)
			if err != nil {
				fmt.Println("Failed, with error =>", err.Error())
				hProcess.ProcessStatusID, hProcess.ErrorMessage = 5, err.Error()
				return hProcess
			}
			// Bufio Writer Match File
			p.mapFileWriter[vToleransi.IDToleransi] = bufio.NewWriter(p.mapOpenFile[vToleransi.IDToleransi])
		}
		fmt.Println("Success")

		for _, vToleransi := range vlevelVolcomp.Toleransi {

			// Opening Undup Tapin File every Level Loop
			// fmt.Printf("      > [ Opening File TAPIN ] : ")
			fmt.Printf("      > [ Using Toleransi Startcall : %d, Toleransi Durasi : %d ] : ", vToleransi.Startcall, vToleransi.Durasi)
			fileUndup, err := os.OpenFile(inputUndup, os.O_RDONLY, os.ModePerm)
			if err != nil {
				// fmt.Println("Failed, with error =>", err.Error())
				hProcess.ProcessStatusID, hProcess.ErrorMessage = 5, err.Error()
				return hProcess
			}
			// fmt.Println("Success")

			p.countInp = 0
			// Looping Undup Tapin File Line by Line
			sc := bufio.NewScanner(fileUndup)
			for sc.Scan() {
				//  Doing Recon Line by Line
				p.countInp++
				if _, ok := p.matchLine[p.countInp]; ok {
					continue
				}

				p.lineRecon(sc, &vlevelVolcomp, writerMatch, urutStartcall, urutDurasi, &vToleransi)
			}
			if err := sc.Err(); err != nil {
				fmt.Println("Failed, with error =>", err.Error())
				hProcess.ProcessStatusID, hProcess.ErrorMessage = 5, err.Error()
				return hProcess
			}

			fileUndup.Close()
			fmt.Println("Done")
		}

		// Closing Match File Per Toleransi
		for _, vToleransi := range vlevelVolcomp.Toleransi {
			err := p.mapFileWriter[vToleransi.IDToleransi].Flush()
			if err != nil {
				fmt.Println("Failed, with error =>", err.Error())
			}
			p.mapOpenFile[vToleransi.IDToleransi].Close()
		}

		for k, v := range p.mapCountMatch {
			k--
			fmt.Printf("      > [ Match Toleransi StartCall : %d, Durasi : %d ] : %d records, %d seconds\n", vlevelVolcomp.Toleransi[k].Startcall, vlevelVolcomp.Toleransi[k].Durasi, v["records"], v["durations"])
		}
	}

	// File Match Bufio Flush
	err = writerMatch.Flush()
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		hProcess.ProcessStatusID, hProcess.ErrorMessage = 5, err.Error()
		return hProcess
	}
	fileMatch.Close()

	fmt.Println("    > [ Total Match ] :", p.countMatch)

	// Writing Tapin Only
	err = p.WritingTapinOnly(inputUndup)
	if err != nil {
		hProcess.ProcessStatusID, hProcess.ErrorMessage = 5, err.Error()
		return hProcess
	}

	err = p.writeMatchKey()
	if err != nil {
		hProcess.ProcessStatusID, hProcess.ErrorMessage = 5, err.Error()
		return hProcess
	}

	for partnerID := range p.mapCountTapinOnly {
		fmt.Println("    > [ Total Tapin Only PartnerID :", partnerID, "] : { Total Records :", p.mapCountTapinOnly[partnerID].Records, "records, Total Durations :", p.mapCountTapinOnly[partnerID].Durations, "seconds }")
	}
	p.countInpTotal = p.countMatch + p.countTapinOnly
	fmt.Println("  > [ Processing Success ] : { Total Input :", p.countInpTotal, ", Match :", p.countMatch, ", Tapin Only :", p.countTapinOnly, "}")
	hProcess.ProcessStatusID, hProcess.TotalInput, hProcess.TotalMatch, hProcess.TotalTapinOnly, hProcess.LogMatch, hProcess.LogTapinOnly, hProcess.LogMatchPartnerID, hProcess.LogMatchToleransiPartnerID = 7, p.countInpTotal, p.countMatch,
		p.countTapinOnly, p.mapCountMatch, p.mapCountTapinOnly, p.mapCountMatchPartnerID, p.mapCountMatchToleransiPartnerID

	// Closing Opened LevelDB
	p.DBOCS.Close()
	p.DBOCSMatch.Close()

	return hProcess
}

func (p *Recon) lineRecon(sc *bufio.Scanner, vlevelVolcomp *model.LevelVolcomp, writerMatch *bufio.Writer, urutStartcall, urutDurasi int, vToleransi *model.ToleransiVolcomp) {
	var keydup string

	columns := strings.Split(strings.Replace(sc.Text(), "+", "", -1), "|")
	for _, vParamCompare := range vlevelVolcomp.Conf["TAPIN"].ParameterCompare {
		urutan, _ := strconv.Atoi(vParamCompare)
		keydup += columns[urutan]
	}

	p.SearchWithTolerance(columns, keydup, writerMatch, sc, urutStartcall, urutDurasi, vlevelVolcomp, vToleransi)

	return
}

// SearchWithTolerance :
func (p *Recon) SearchWithTolerance(columns []string, keydup string, writerMatch *bufio.Writer, sc *bufio.Scanner, urutStartcall, urutDurasi int, vlevelVolcomp *model.LevelVolcomp, vToleransi *model.ToleransiVolcomp) {

	startcallColumns, err := common.ParseDatetime("20060102150405", columns[urutStartcall])
	if err != nil {
		fmt.Println(err.Error())
		return

	}

	durColumns, _ := strconv.Atoi(columns[urutDurasi])
	operatorID := columns[p.DataStructure.TypeData["TAPIN"].Parameter["PARTNERID"].Urutan]

	revenueTAPIN, _ := strconv.ParseFloat(columns[p.DataStructure.TypeData["TAPIN"].Parameter["TAP_FEE"].Urutan], 64)

	// for _, vToleransi := range vlevelVolcomp.Toleransi {
	iter := p.DBOCS.NewIterator(util.BytesPrefix([]byte(keydup+"-")), nil)

	for iter.Next() {
		// Splitting OCS Key, Form : { ImsiBpartyTapcode-Startcall-Duration-Matchpath }
		splitKey := strings.Split(string(iter.Key()), "-")
		keyStruct := KeyStruct{
			KeyCompare:         splitKey[0],
			StartCall:          splitKey[1],
			Duration:           splitKey[2],
			MatchPath:          splitKey[3],
			KeyExceptMatchPath: strings.Join(splitKey[:len(splitKey)-1], "-"),
		}

		// Check IF OCS already has Match On Current Process
		if p.matchKey[keyStruct.MatchPath][keyStruct.KeyExceptMatchPath] > 0 {
			continue
		}

		// Check IF OCS already has Match on Match Database
		exists, _ := p.DBOCSMatch.Has(iter.Key(), nil)
		if exists == true {
			continue
		}

		// Parsing StartCall and skip to next key if error
		startcallKey, err := common.ParseDatetime("20060102150405", keyStruct.StartCall)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		// Calculate StartCall difference in seconds and skip to next key If Difference > Toleration
		startcallDiff := math.Abs(startcallColumns.Sub(startcallKey).Seconds())
		if int(startcallDiff) > vToleransi.Startcall {
			continue
		}

		// Calculate Duration difference and skip to next key If Difference > Toleration
		durKey, _ := strconv.Atoi(keyStruct.Duration)
		durationDiff := math.Abs(float64(durColumns) - float64(durKey))
		if int(durationDiff) > vToleransi.Durasi {
			continue
		}

		// If Startcall Difference and Duration Difference Valid, Execute OnMatchLine Function
		valOCS := fmt.Sprintf("%s|%d|%d", string(iter.Value()), vToleransi.Startcall, vToleransi.Durasi)
		p.OnMatchLine(&keyStruct, writerMatch, vToleransi.IDToleransi, durColumns, sc.Text(), valOCS, operatorID, revenueTAPIN)

		iter.Release()

		return
	}
	iter.Release()
	// }

	return
}

// OnMatchLine :
func (p *Recon) OnMatchLine(keyStruct *KeyStruct, writerMatch *bufio.Writer, idToleransi, durColumns int, valTAPIN, valOCS, operatorID string, revenueTAPIN float64) int {
	p.countMatch++

	// Counting Match By ID Toleransi
	if p.mapCountMatch[idToleransi] == nil {
		p.mapCountMatch[idToleransi] = map[string]int{}
	}

	p.mapCountMatch[idToleransi]["durations"] += durColumns

	p.mapCountMatch[idToleransi]["records"]++

	// Counting Match By Partner ID
	if p.mapCountMatchPartnerID[operatorID] == nil {
		p.mapCountMatchPartnerID[operatorID] = &model.LogMatchContent{}
	}

	p.mapCountMatchPartnerID[operatorID].Durations += durColumns

	p.mapCountMatchPartnerID[operatorID].Revenues += revenueTAPIN

	p.mapCountMatchPartnerID[operatorID].Records++

	if p.mapCountMatchToleransiPartnerID[operatorID] == nil {
		p.mapCountMatchToleransiPartnerID[operatorID] = make(map[int]*model.LogMatchContent)
	}

	if p.mapCountMatchToleransiPartnerID[operatorID][idToleransi] == nil {
		p.mapCountMatchToleransiPartnerID[operatorID][idToleransi] = &model.LogMatchContent{}
	}

	p.mapCountMatchToleransiPartnerID[operatorID][idToleransi].Durations += durColumns

	p.mapCountMatchToleransiPartnerID[operatorID][idToleransi].Revenues += revenueTAPIN

	p.mapCountMatchToleransiPartnerID[operatorID][idToleransi].Records++

	// Flagging match line
	p.matchLine[p.countInp] = 1
	if p.matchKey[keyStruct.MatchPath] == nil {
		p.matchKey[keyStruct.MatchPath] = map[string]int{}
	}
	p.matchKey[keyStruct.MatchPath][keyStruct.KeyExceptMatchPath]++

	fmt.Fprintln(writerMatch, valTAPIN+"|"+valOCS)
	fmt.Fprintln(p.mapFileWriter[idToleransi], valTAPIN+"|"+valOCS)

	return 1
}

// WritingTapinOnly :
func (p *Recon) WritingTapinOnly(inputUndup string) error {
	fmt.Println("  > [ Writing TAPIN Only ]")

	p.mapCountTapinOnly = make(map[string]*model.LogMatchContent)

	currentDataStructure := p.DataStructure.TypeData["TAPIN"]

	// Making Tapin Only Directories if not exists
	if _, err := os.Stat(p.ListProcess.PathOnly); os.IsNotExist(err) {
		fmt.Printf("    > [ Making TAPIN Only Directory ] : ")
		err = os.MkdirAll(p.ListProcess.PathOnly, 0777)
		if err != nil {
			fmt.Println("Failed, with error =>", err.Error())
			return err
		}
		fmt.Println("Success")
	}

	// Opening & Creating Tapin Only File
	inputOnly := p.ListProcess.PathOnly + p.ListProcess.FilenameOnly
	fmt.Println("    > [ TAPIN Only File ] : " + inputOnly)
	fileOnly, err := os.Create(inputOnly)
	if err != nil {
		fmt.Println("    > Failed, with error =>", err.Error())
		return err
	}
	// Bufio Writer Tapin Only File
	writerOnly := bufio.NewWriter(fileOnly)
	// Opening Undup File
	fileUndup, err := os.OpenFile(inputUndup, os.O_RDONLY, os.ModePerm)
	if err != nil {
		fmt.Println("    > Failed, with error =>", err.Error())
		return err
	}
	defer fileUndup.Close()

	// Looping Undup Tapin File Line by Line
	sc := bufio.NewScanner(fileUndup)
	currLine := 0
	for sc.Scan() {
		currLine++
		// Doing Write Tapin Only for line that has no matchLine
		if _, ok := p.matchLine[currLine]; ok {
			continue
		}
		// Splitting current line to get field array
		columns := strings.Split(strings.Replace(sc.Text(), "+", "", -1), "|")

		calledNumber := columns[currentDataStructure.Parameter["OTHERTELNUM"].Urutan]
		operatorID := columns[currentDataStructure.Parameter["PARTNERID"].Urutan]
		callingNumber := columns[currentDataStructure.Parameter["MSISDN"].Urutan]
		tapFileName := columns[currentDataStructure.Parameter["TAP_FILENAME"].Urutan]
		sessionID := callingNumber + calledNumber + tapFileName + columns[currentDataStructure.Parameter["STARTTIME"].Urutan]
		utcTime := "+" + columns[currentDataStructure.Parameter["UTC_TIME"].Urutan]
		t, _ := time.Parse("20060102150405", columns[currentDataStructure.Parameter["STARTTIME"].Urutan])
		timeFormat := t.Format("2006/01/02-15-04-05")

		m := []string{
			0:  `"` + "Event: " + columns[currentDataStructure.Parameter["IMSI"].Urutan] + `"`,
			1:  `"` + "10" + `"`,
			2:  `"` + timeFormat + `"`,
			3:  `""`,
			4:  `""`,
			5:  `""`,
			6:  `""`,
			7:  `""`,
			8:  `""`,
			9:  `""`,
			10: `""`,
			11: `""`,
			12: `""`,
			13: `"` + calledNumber + `"`,
			14: `"` + operatorID + `"`,
			15: `"` + columns[currentDataStructure.Parameter["SERVICENAME"].Urutan] + `"`,
			16: `"` + columns[currentDataStructure.Parameter["TAP_FEE"].Urutan] + `"`,
			17: `"` + callingNumber + `"`,
			18: `""`,
			19: `"6"`,
			20: `"` + columns[currentDataStructure.Parameter["CALLEDDIGIT"].Urutan] + `"`,
			21: `"` + columns[currentDataStructure.Parameter["DURATION"].Urutan] + `"`,
			22: `"` + columns[currentDataStructure.Parameter["MSISDN"].Urutan] + `"`,
			23: `"` + sessionID + `"`,
			24: `""`,
			25: `"1"`,
			26: `"` + operatorID + `"`,
			// 27: `"` + columns[currentDataStructure.Parameter["STARTTIME"].Urutan] + `"`,
			27: `"` + columns[currentDataStructure.Parameter["LOCAL_TIME"].Urutan] + `"`,
			28: `""`,
			29: `""`,
			30: `""`,
			31: `""`,
			32: `"` + utcTime + `"`,
			33: `""`,
			34: `""`,
			35: `""`,
			36: `""`,
			37: `"` + tapFileName + `"`,
		}
		// Write Line to file Tapin Only
		fmt.Fprintln(writerOnly, strings.Join(m, ","))

		if p.mapCountTapinOnly[operatorID] == nil {
			p.mapCountTapinOnly[operatorID] = &model.LogMatchContent{}
		}

		dur, _ := strconv.Atoi(columns[currentDataStructure.Parameter["DURATION"].Urutan])

		revenueTAPIN, _ := strconv.ParseFloat(columns[currentDataStructure.Parameter["TAP_FEE"].Urutan], 64)

		p.mapCountTapinOnly[operatorID].Durations += dur
		p.mapCountTapinOnly[operatorID].Revenues += revenueTAPIN

		p.mapCountTapinOnly[operatorID].Records++

		p.countTapinOnly++
	}
	if err := sc.Err(); err != nil {
		fmt.Println("    > Failed, with error =>", err.Error())
		return err
	}

	// File Tapin Only Bufio Flush
	err = writerOnly.Flush()
	if err != nil {
		fmt.Println("    > Failed, with error =>", err.Error())
		return err
	}
	fileOnly.Close()

	return nil
}

func (p *Recon) writeMatchKey() error {
	fmt.Println("  > [ Writing Match Key To DB ]")
	// Looping every OCS database
	for dbPath := range p.matchKey {
		fmt.Printf("    > [ Match DB Path ] : " + dbPath)
		// Opening Match Database
		dBConn, err := leveldb.OpenFile(dbPath, nil)
		if err != nil {
			fmt.Println("Failed To Open LevelDB, with error =>", err.Error())
			return err
		}
		// Looping Match Key
		for k, v := range p.matchKey[dbPath] {
			// Putting Match Key To Match Database
			err = dBConn.Put([]byte(k), []byte(strconv.Itoa(v)), nil)
		}
		// Close Match Database
		dBConn.Close()

		fmt.Println(" => Done")
	}
	fmt.Println("    > [ Done ]")

	return nil
}
