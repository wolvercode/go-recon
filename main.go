package main

import (
	"bitbucket.org/billing/go-recon/app"
	"bitbucket.org/billing/go-recon/flow"
)

func main() {
	for {
		app.Initialize()

		flow.MainRecon()
		// flow.TapinOnlyRecon()

		app.Sleeping()
	}
}

func doMainRecon() chan bool {
	done := make(chan bool)
	go func() {
		flow.MainRecon()
		done <- true
	}()

	return done
}

func doTapinOnlyRecon() chan bool {
	done := make(chan bool)
	go func() {
		flow.TapinOnlyRecon()
		done <- true
	}()

	return done
}
