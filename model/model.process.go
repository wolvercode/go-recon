package model

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"bitbucket.org/billing/go-recon/app"
)

// GetProcess :
func (p *ProcessRecon) GetProcess() ([]ProcessRecon, error) {
	var listProcess []ProcessRecon

	response, err := http.Get(app.Appl.DBAPIURL + "process/recon/getProcess")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&listProcess)

	return listProcess, nil
}

// UpdateStatus :
func (p *ProcessRecon) UpdateStatus(processID, processStatusID string) error {
	client := &http.Client{}
	response, err := http.NewRequest(http.MethodPut, app.Appl.DBAPIURL+"process/updateStatus/"+processID+"/"+processStatusID, bytes.NewBuffer(nil))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	_, err = client.Do(response)
	if err != nil {
		// handle error
		log.Fatal(err)
	}
	defer io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}

// GetDataStructure :
func (p *ProcessRecon) GetDataStructure() (DataStructure, error) {
	var dataStructure DataStructure

	response, err := http.Get(app.Appl.DBAPIURL + "configuration/getDataStructure")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return dataStructure, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&dataStructure)

	return dataStructure, nil
}

// GetSlicedProcess :
func (p *ProcessRecon) GetSlicedProcess(minStartcall, maxStartcall, dataTypeID, moMt, postPre string) ([]SlicedProcess, error) {
	var listPathDB []SlicedProcess

	response, err := http.Get(app.Appl.DBAPIURL + "process/getSlicedProcess/" + minStartcall + "/" + maxStartcall + "/" + dataTypeID + "/" + moMt + "/" + postPre)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&listPathDB)

	return listPathDB, nil
}

// GetLevelVolcomp :
func (p *ProcessRecon) GetLevelVolcomp() ([]LevelVolcomp, error) {
	var levelVolcomp []LevelVolcomp

	response, err := http.Get(app.Appl.DBAPIURL + "process/recon/getLevelVolcomp")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&levelVolcomp)

	return levelVolcomp, nil
}

// CloseProcess :
func (p *ProcessRecon) CloseProcess(closeStruct *ClosingStruct) error {
	jsonValue, _ := json.Marshal(closeStruct)
	response, err := http.Post(app.Appl.DBAPIURL+"process/recon/closeProcess/", "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}

// GetProcessTapinOnly :
func (p *ProcessRecon) GetProcessTapinOnly() ([]ProcessReconTapinOnly, error) {
	var listProcess []ProcessReconTapinOnly

	response, err := http.Get(app.Appl.DBAPIURL + "process/recontapinonly/getProcess")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&listProcess)

	return listProcess, nil
}

// GetListTapinOnly :
func (p *ProcessRecon) GetListTapinOnly(periode, moMt, postPre string) ([]ListTapinOnly, error) {
	var listProcess []ListTapinOnly

	response, err := http.Get(app.Appl.DBAPIURL + "process/recontapinonly/getListTapinOnly/" + periode + "/" + moMt + "/" + postPre + "/")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&listProcess)

	return listProcess, nil
}

// GetLevelVolcompTapinOnly :
func (p *ProcessRecon) GetLevelVolcompTapinOnly() ([]LevelVolcomp, error) {
	var levelVolcomp []LevelVolcomp

	response, err := http.Get(app.Appl.DBAPIURL + "process/recontapinonly/getLevelVolcomp")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&levelVolcomp)

	return levelVolcomp, nil
}
