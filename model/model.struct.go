package model

// DataStructure :
type DataStructure struct {
	TypeData map[string]DataStructureParameter `json:"data_type"`
}

// DataStructureParameter :
type DataStructureParameter struct {
	Parameter map[string]DataStructureProperties `json:"parameter"`
}

// DataStructureProperties :
type DataStructureProperties struct {
	Urutan     int `json:"urutan"`
	SortUrutan int `json:"sort_urutan"`
}

// ProcessRecon :
type ProcessRecon struct {
	ProcessID     string `json:"process_id"`
	ProcesstypeID string `json:"processtype_id"`
	BatchID       string `json:"batch_id"`
	DataTypeID    int    `json:"data_type_id"`
	Periode       string `json:"periode"`
	DayPeriode    string `json:"day"`
	MoMt          string `json:"mo_mt"`
	PostPre       string `json:"post_pre"`
	UndupID       int    `json:"id_undup"`
	PathUndup     string `json:"path_undup"`
	FilenameUndup string `json:"filename_undup"`
	OnlyID        int    `json:"only_id"`
	PathOnly      string `json:"path_only"`
	FilenameOnly  string `json:"filename_only"`
	MatchID       int    `json:"match_id"`
	PathMatch     string `json:"path_match"`
	FilenameMatch string `json:"filename_match"`
	MaxDelay      string `json:"max_delay"`
	MinStartcall  string `json:"min_startcall"`
	MaxStartcall  string `json:"max_startcall"`
}

// SlicedProcess :
type SlicedProcess struct {
	PathDB string `json:"path_db"`
}

// ClosingStruct :
type ClosingStruct struct {
	ProcessConf                *ProcessRecon                       `json:"process_conf"`
	TotalInput                 int                                 `json:"total_input"`
	TotalMatch                 int                                 `json:"total_match"`
	TotalTapinOnly             int                                 `json:"total_tapin_only"`
	ProcessStatusID            int                                 `json:"process_status_id"`
	ErrorMessage               string                              `json:"error_message"`
	LogMatch                   map[int]map[string]int              `json:"log_match"`
	LogTapinOnly               map[string]*LogMatchContent         `json:"log_tapinonly"`
	LogMatchPartnerID          map[string]*LogMatchContent         `json:"log_match_partnerid"`
	LogMatchToleransiPartnerID map[string]map[int]*LogMatchContent `json:"log_match_toleransi_partnerid"`
}

// LogMatchContent :
type LogMatchContent struct {
	Durations int     `json:"durations"`
	Records   int     `json:"records"`
	Revenues  float64 `json:"revenues"`
}

// LevelVolcomp :
type LevelVolcomp struct {
	Level     int                         `json:"level"`
	Conf      map[string]LevelVolcompConf `json:"conf"`
	Toleransi []ToleransiVolcomp          `json:"toleransi"`
}

// LevelVolcompConf :
type LevelVolcompConf struct {
	IDLevel            int      `json:"id_level"`
	Level              int      `json:"level"`
	Type               string   `json:"type"`
	ParameterCompare   []string `json:"parameter_compare"`
	ParameterToleransi []string `json:"parameter_toleransi"`
}

// ToleransiVolcomp :
type ToleransiVolcomp struct {
	IDToleransi int `json:"id_toleransi"`
	Level       int `json:"level"`
	Startcall   int `json:"startcall"`
	Durasi      int `json:"durasi"`
	Priorities  int `json:"priorities"`
	ActiveID    int `json:"active_id"`
}

// ProcessReconTapinOnly :
type ProcessReconTapinOnly struct {
	ProcessID              string `json:"process_id"`
	BatchID                string `json:"batch_id"`
	Periode                string `json:"periode"`
	DataTypeID             int    `json:"data_type_id"`
	MoMt                   string `json:"mo_mt"`
	PostPre                string `json:"post_pre"`
	MatchTapinOnlyID       int    `json:"match_tapin_only_id"`
	MatchTapinOnlyPath     string `json:"match_tapin_only_path"`
	MatchTapinOnlyFilename string `json:"match_tapin_only_filename"`
	OnlyTapinOnlyID        int    `json:"only_tapin_only_id"`
	OnlyTapinOnlyPath      string `json:"only_tapin_only_path"`
	OnlyTapinOnlyFilename  string `json:"only_tapin_only_filename"`
	MinStartcall           string `json:"min_startcall"`
	MaxStartcall           string `json:"max_startcall"`
}

// ListTapinOnly :
type ListTapinOnly struct {
	FileID      int    `json:"file_id"`
	Periode     string `json:"periode"`
	BatchIDAsal string `json:"batch_id_asal"`
	FileTypeID  int    `json:"file_type_id"`
	PathFile    string `json:"pathfile"`
	Filename    string `json:"filename"`
	MoMt        string `json:"mo_mt"`
	PostPre     string `json:"post_pre"`
}
