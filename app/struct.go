package app

// Config : Struct of App Config
type Config struct {
	Active string
	Dev    ConfigStruct
	Prod   ConfigStruct
}

// ConfigStruct : Struct of App Config's Item
type ConfigStruct struct {
	Main    MainStruct
	RestAPI RestAPIStruct
	DbAPI   DbAPIStruct
}

// MainStruct : Struct of App Main Config
type MainStruct struct {
	DisplayName string
	ProcessDir  string
	Sleep       int
}

// RestAPIStruct : Struct of RestAPI Config
type RestAPIStruct struct {
	Host string
	Port int
	URL  string
}

// DbAPIStruct : Struct of DB Config
type DbAPIStruct struct {
	Host string
	Port int
	URL  string
}
